<?php

include 'person.php';

class Book{

    public $price;
    public $author;

    public function price(int $price){
        $this->price = $price;
    }

     public function authors(Person $person){
        $this->author = $person->names();
    }
}

$book = new Book();
$authorsName = new Person();
// $book->price(100);
$book->authors($authorsName);
// echo $book->price."<br>";
print_r($book->author) ;
// echo 'hello';