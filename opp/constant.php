<?php 
class Fruits{
    public $name;
    const Color = "Red";
    public function setName($name){
        $this->name = $name;
    }
}

class Apples extends Fruits{
    public function juice(){
        echo "Apple juice"."<br>";
    }

    public function color(){
        echo "Apple color ".Fruits::Color;
    }
}

class Mangos extends Fruits{

}

$apple = new Apples();

$apple->setName('Apple');
    echo "Fruit name ". $apple->name."<br>";
$apple->juice();
$apple->color();

?>