<?php 
class Fruits{
    public $name;
    const Color = "Red";
    public static $weight = 22;
    public function setName($name){
        $this->name = $name;
    }
}

class Apples extends Fruits{
    public function juice(){
        echo "Apple juice"."<br>";
    }

    public function color(){
        echo "Apple color ".Fruits::Color."<br>";
    }
    public function weight(){
        echo "Apple weight ".self::$weight."<br>";
    }
}

class Mangos extends Fruits{

}

$apple = new Apples();

$apple->setName('Apple');
    echo "Fruit name ". $apple->name."<br>";
$apple->juice();
$apple->color();
$apple->weight();

?>