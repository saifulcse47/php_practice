<?php

class Bill{
    public $dinner = 20;
    public $dessert = 10;
    public $coldDrink = 5;
    public $bill;

    public function __construct() {
        $this->bill = 3;
    }

    public function dinner($person){
       $this->bill += $this->dinner * $person;
       return $this;
    }
    public function dessert($person){
       $this->bill += $this->dessert * $person;
       return $this;
    }
    public function coldDrink($person){
       $this->bill += $this->coldDrink * $person;
       return $this;
    }

    public function __destruct() {
        echo "Total bill is ". $this->bill;
    }
}

$bill = new Bill();

  $bill->dinner(2)->dessert(2)->coldDrink(1)->bill;

?>