<?php
// namespace second;
include 'fns.php';
include 'sns.php';
use second\A as newA;

// $object = new second\A; // qualiffied class name.

$object = new newA; // unqualified class name.
echo "<br>";
$object = new \A; // unqualified class name.