<?php include('include/header.php') ?>
<?php include('method/formula.php') ?>

<div class="container">
    <div class="row">
        <div class="col-md-6 m-auto">
            <form class="" action="" method="post" enctype="multipart/form">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">First Number</label>
                    <input type="number" name="num1" class="form-control" >
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Secend Number</label>
                    <input type="number" name="num2" class="form-control" >
                </div>
                <button type="submit" name="calculation" class="btn btn-primary">Calculate</button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="m-auto">
            <?php 
            if(isset($_POST['calculation'])) {
                $numOne = $_POST['num1'];
                $numTwo = $_POST['num2'];

                if(empty($numOne) or empty($numTwo)) {
                    echo "<span style='' >Filed must be not empty</span>";
                }
                else{
                    echo "First value is ".$numOne." and second value is ".$numTwo."<br>";
                    $cal = new Calculation();
                    $cal->add($numOne, $numTwo);
                    $cal->sub($numOne, $numTwo);
                    $cal->multiple($numOne, $numTwo);
                    $cal->division($numOne, $numTwo);
                }
            }
        ?>
        </div>
    </div>
</div>


